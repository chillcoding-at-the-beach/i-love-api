defmodule ILoveApi.Accounts.AccountsTest do
  use ILoveApi.DataCase

  alias ILoveApi.Accounts
  alias ILoveApi.Devices

  alias ILoveApi.Accounts.User

  @uuid "uuid"
  @username "some username"
  @new_username "some updated username"
  @valid_attrs %{username: @username}
  @update_attrs %{username: @new_username}
  @invalid_attrs %{username: nil}

  def device_fixture(attrs \\ %{}) do
    {:ok, device} =
      attrs
      |> Enum.into(%{uuid: @uuid})
      |> Devices.create_device()

    device
  end

  def user_fixture(attrs \\ %{}) do
    device = device_fixture()
    base_attrs = Map.merge(@valid_attrs, %{device_id: device.id})

    {:ok, user} =
      attrs
      |> Enum.into(base_attrs)
      |> Accounts.create_user()

    user
    |> Repo.preload([:device, :scores])
  end

  test "list_users/0 returns all users" do
    user = user_fixture()

    assert Accounts.list_users() == [user]
  end

  test "get_user!/1 returns the user with given id" do
    user = user_fixture()
    assert Accounts.get_user!(user.id) == user
  end

  test "get_user!/1 with invalid id" do
    assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(42) end
  end

  test "create_user/1 with valid data creates a user" do
    device = device_fixture()
    base_attrs = Map.merge(@valid_attrs, %{device_id: device.id})

    assert {:ok, %User{} = user} = Accounts.create_user(base_attrs)
    assert user.username == @username
  end

  test "create_user/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
  end

  test "get_or_create_user_by_username/2 with existing user retrieves the user" do
    user = user_fixture()

    assert {:ok, %User{} = found_user} = Accounts.get_or_create_user_by_username(user.username, @uuid)

    assert found_user == user
  end

  test "get_or_create_user_by_username/2 with wrong uuid returns an error" do
    user = user_fixture()

    assert {:error, :wrong_uuid} = Accounts.get_or_create_user_by_username(user.username, "wrong uuid")
  end

  test "get_or_create_user_by_username/2 with non-existing user creates a new user" do
    assert {:ok, %User{} = new_user} = Accounts.get_or_create_user_by_username(@username, @uuid)

    assert new_user.username == @username
    assert new_user.device.uuid == @uuid
  end

  test "get_or_create_user_by_username/2 with a user already associated with the uuid creates a new user" do
    user = user_fixture()

    assert {:ok, %User{} = new_user} = Accounts.get_or_create_user_by_username(@new_username, @uuid)

    assert new_user.id != user.id
    assert new_user.username == @new_username
    assert new_user.device.uuid == @uuid
  end

  test "update_user/2 with valid data updates the user" do
    user = user_fixture()

    assert {:ok, user} = Accounts.update_user(user, @update_attrs)
    assert %User{} = user
    assert user.username == "some updated username"
  end

  test "update_user/2 with invalid data returns error changeset" do
    user = user_fixture()

    assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
    assert user == Accounts.get_user!(user.id)
  end

  test "delete_user/1 deletes the user" do
    user = user_fixture()

    assert {:ok, %User{}} = Accounts.delete_user(user)
    assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
  end

  test "change_user/1 returns a user changeset" do
    assert %Ecto.Changeset{} = Accounts.change_user(@valid_attrs)
  end

  test "change_user/2 returns a user changeset with a user given" do
    user = user_fixture()

    assert %Ecto.Changeset{} = Accounts.change_user(user, @update_attrs)
  end
end
