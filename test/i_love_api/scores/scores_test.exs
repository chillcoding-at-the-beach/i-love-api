defmodule ILoveApiWeb.ScoresTest do
  use ILoveApi.DataCase

  alias ILoveApi.Scores
  alias ILoveApi.Accounts
  alias ILoveApi.Scores.Score

  @valid_attrs %{point: 42}
  @update_attrs %{point: 43}
  @invalid_attrs %{point: nil}

  def score_fixture(attrs \\ %{}) do
    user = user_fixture()
    base_attrs = Map.merge(@valid_attrs, %{user_id: user.id})

    {:ok, score} = Scores.create_score(user, Enum.into(attrs, base_attrs))

    score
  end

  def user_fixture do
    {:ok, user} = Accounts.get_or_create_user_by_username("johndoe", "uuid")
    user
  end

  test "list_scores/0 returns all scores" do
    score = score_fixture()

    assert Scores.list_scores() == [score]
  end

  test "get_score!/1 returns the score with given id" do
    score = score_fixture()

    assert Scores.get_score!(score.id) == score
  end

  test "create_score/1 with valid data creates a score" do
    user = user_fixture()
    attrs = Map.merge(@valid_attrs, %{user_id: user.id})

    assert {:ok, %Score{} = score} = Scores.create_score(attrs)
    assert score.point == 42
  end

  test "create_score/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Scores.create_score(@invalid_attrs)
  end

  test "create_score/2 with valid data creates a score" do
    user = user_fixture()

    assert {:ok, %Score{} = score} = Scores.create_score(user, @valid_attrs)
    assert score.point == 42
  end

  test "update_score/2 with valid data updates the score" do
    score = score_fixture()

    assert {:ok, score} = Scores.update_score(score, @update_attrs)
    assert %Score{} = score
    assert score.point == 43
  end

  test "update_score/2 with invalid data returns error changeset" do
    score = score_fixture()

    assert {:error, %Ecto.Changeset{}} = Scores.update_score(score, @invalid_attrs)
    assert score == Scores.get_score!(score.id)
  end

  test "delete_score/1 deletes the score" do
    score = score_fixture()

    assert {:ok, %Score{}} = Scores.delete_score(score)
    assert_raise Ecto.NoResultsError, fn -> Scores.get_score!(score.id) end
  end

  test "change_score/1 returns a score changeset" do
    score = score_fixture()

    assert %Ecto.Changeset{} = Scores.change_score(score)
  end
end
