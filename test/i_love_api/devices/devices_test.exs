defmodule ILoveApi.Devices.DevicesTest do
  use ILoveApi.DataCase

  alias ILoveApi.Devices
  alias ILoveApi.Devices.Device

  @uuid "some uuid"
  @valid_attrs %{uuid: @uuid}
  @invalid_attrs %{uuid: nil}

  def device_fixture(attrs \\ %{}) do
    {:ok, device} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Devices.create_device()

    device
  end

  defp setup_device(_) do
    [device: device_fixture()]
  end

  test "Devices.create_device/1 with valid data creates a device" do
    assert {:ok, %Device{} = device} = Devices.create_device(@valid_attrs)
    assert device.uuid == "some uuid"
  end

  test "Devices.create_device/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Devices.create_device(@invalid_attrs)
  end

  describe "Devices.get_or_create_device_by_uuid/1 with an existing device" do
    setup [:setup_device]

    test "retrieves the device", %{device: device} do
      assert {:ok, %Device{} = device_found} = Devices.get_or_create_device_by_uuid(@uuid)
      assert device.uuid == device_found.uuid
    end

    test "creates a new device", %{device: device} do
      assert {:ok, %Device{} = new_device} = Devices.get_or_create_device_by_uuid("foo")
      assert new_device.id != device.id
      assert new_device.uuid == "foo"
    end
  end
end
