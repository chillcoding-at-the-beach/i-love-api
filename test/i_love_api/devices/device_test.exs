defmodule ILoveApi.Devices.DeviceTest do
  use ILoveApi.DataCase

  alias ILoveApi.Devices.Device

  @valid_attrs %{uuid: "some uuid"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Device.changeset(%Device{}, @valid_attrs)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Device.changeset(%Device{}, @invalid_attrs)

    refute changeset.valid?
  end
end
