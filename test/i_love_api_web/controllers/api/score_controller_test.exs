defmodule ILoveApiWeb.Api.ScoreControllerTest do
  use ILoveApiWeb.ConnCase

  alias ILoveApi.Accounts
  alias ILoveApi.Accounts.User
  alias ILoveApi.Devices.Device
  alias ILoveApi.Scores
  alias ILoveApi.Scores.Score
  alias ILoveApi.Repo
  alias Ecto.Query
  import ILoveApiWeb.Router.Helpers

  @uuid "1111"
  @username "some username"
  @point 42
  @create_attrs %{device: %{uuid: @uuid}, user: %{username: @username}, score: %{point: @point}}

  @basic_auth_username Application.get_env(:i_love_api, :admin_basic_auth)[:username]
  @basic_auth_password Application.get_env(:i_love_api, :admin_basic_auth)[:password]

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp using_basic_auth(conn, username, password) do
    header_content = "Basic " <> Base.encode64("#{username}:#{password}")
    put_req_header(conn, "authorization", header_content)
  end

  def fixture(:score) do
    {:ok, score} = Scores.create_score(fixture(:user), Map.get(@create_attrs, :score))
    score
  end

  def fixture(:user) do
    {:ok, user} = Accounts.get_or_create_user_by_username(@username, @uuid)
    user
  end

  defp setup_basic_auth(context) do
    conn = context[:conn]
      |> using_basic_auth(@basic_auth_username, @basic_auth_password)
    [conn: conn]
  end

  defp setup_user(_) do
    [user: fixture(:user)]
  end

  defp setup_score(_) do
    [score: fixture(:score)]
  end

  describe "GET /scores without basic auth credentials and a score" do
    setup [:setup_score]

    test "returns all scores", %{conn: conn} do
      conn = conn
        |> get(api_score_path(conn, :index))

      body = json_response(conn, 200)
      first_result = List.first(body)

      assert length(body) == 1
      assert first_result["device"]["uuid"] == @uuid
      assert first_result["user"]["username"] == @username
      assert first_result["point"] == 42
    end
  end

  describe "POST /scores without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> post(api_score_path(conn, :create), @create_attrs)

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "POST /scores with basic auth credentials" do
    setup [:setup_basic_auth]

    test "creates user and score when data is valid", %{conn: conn} do
      conn = conn
        |> post(api_score_path(conn, :create), @create_attrs)
      body = json_response(conn, 201)

      score = Repo.one!(Query.first(Score))

      assert body["id"] == score.id
      assert body["point"] == @point
      assert body["device"]["id"] == Repo.get_by!(Device, uuid: @uuid).id
      assert body["device"]["uuid"] == @uuid
      assert body["user"]["id"] == Repo.get_by!(User, username: @username).id
      assert body["user"]["username"] == @username
      assert body["_links"]["self"] == api_score_path(conn, :show, score)
    end

    test "returns errors when uuid is nil", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{device: %{uuid: nil}})
      conn = conn
        |> post(api_score_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "device" => ["uuid can't be blank"],
        }
      } = body
    end

    test "returns errors when username is nil", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{user: %{username: nil}})
      conn = conn
        |> post(api_score_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "user" => ["username can't be blank"],
        }
      } = body
    end

    test "returns errors when point is nil", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{score: %{point: nil}})
      conn = conn
        |> post(api_score_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "score" => ["point can't be blank"],
        }
      } = body
    end
  end

  describe "POST /scores with basic auth credentials and an existing user" do
    setup [:setup_user, :setup_basic_auth]

    test "creates score when data is valid", %{conn: conn, user: user} do
      conn = conn
        |> post(api_score_path(conn, :create), @create_attrs)
      body = json_response(conn, 201)

      score = Repo.one!(Query.first(Score))

      assert body["id"] == score.id
      assert body["point"] == @point
      assert body["device"]["id"] == Repo.get_by!(Device, uuid: @uuid).id
      assert body["device"]["uuid"] == @uuid
      assert body["user"]["id"] == user.id
      assert body["user"]["username"] == @username
      assert body["_links"]["self"] == api_score_path(conn, :show, score)
    end

    test "returns errors when username is already taken", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{device: %{uuid: "another uuid"}})
      conn = conn
        |> post(api_score_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "user" => ["username is already taken"]
        }
      } = body
    end
  end
end
