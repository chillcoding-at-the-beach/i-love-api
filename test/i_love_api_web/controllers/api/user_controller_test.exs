defmodule ILoveApiWeb.Api.UserControllerTest do
  use ILoveApiWeb.ConnCase

  alias ILoveApi.Accounts
  alias ILoveApi.Accounts.User
  alias ILoveApi.Devices.Device
  alias ILoveApi.Repo
  alias Ecto.Query

  @uuid "1111"
  @username "some username"
  @new_username "another username"
  @create_attrs %{device: %{uuid: @uuid}, user: %{username: @username}}

  @basic_auth_username Application.get_env(:i_love_api, :admin_basic_auth)[:username]
  @basic_auth_password Application.get_env(:i_love_api, :admin_basic_auth)[:password]

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp using_basic_auth(conn, username, password) do
    header_content = "Basic " <> Base.encode64("#{username}:#{password}")
    put_req_header(conn, "authorization", header_content)
  end

  def fixture(:user) do
    {:ok, user} = Accounts.get_or_create_user_by_username(@username, @uuid)
    user
  end

  defp setup_basic_auth(context) do
    conn = context[:conn]
      |> using_basic_auth(@basic_auth_username, @basic_auth_password)
    [conn: conn]
  end

  defp setup_user(_) do
    [user: fixture(:user)]
  end

  describe "POST /users/:id without basic auth credentials" do
    setup [:setup_user]

    test "prevents access", %{conn: conn} do
      conn = conn
        |> post(api_user_path(conn, :create, @create_attrs))

      assert response(conn, 401)
    end
  end

  describe "POST /users with basic auth credentials" do
    setup [:setup_basic_auth]

    test "creates a new user", %{conn: conn} do
      conn = conn
        |> post(api_user_path(conn, :create), @create_attrs)
      body = json_response(conn, 200)

      new_user = Repo.one!(Query.last(User))

      assert body["id"] == new_user.id
      assert body["username"] == @username
      assert body["device"]["id"] == Repo.get_by!(Device, uuid: @uuid).id
      assert body["device"]["uuid"] == @uuid
      assert body["_links"]["self"] == api_user_path(conn, :show, new_user)
    end

    test "returns errors when uuid is nil", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{device: %{uuid: nil}})
      conn = conn
        |> post(api_user_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "device" => ["uuid can't be blank"],
        }
      } = body
    end

    test "returns errors when username is nil", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{user: %{username: nil}})
      conn = conn
        |> post(api_user_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "user" => ["username can't be blank"],
        }
      } = body
    end
  end

  describe "POST /users with basic auth credentials and an existing user" do
    setup [:setup_user, :setup_basic_auth]

    test "creates a new user", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{user: %{username: @new_username}})
      conn = conn
        |> post(api_user_path(conn, :create), create_attrs)
      body = json_response(conn, 200)

      new_user = Repo.one!(Query.last(User, :inserted_at))

      assert body["id"] == new_user.id
      assert body["username"] == @new_username
      assert body["device"]["id"] == Repo.get_by!(Device, uuid: @uuid).id
      assert body["device"]["uuid"] == @uuid
      assert body["_links"]["self"] == api_user_path(conn, :show, new_user)
    end

    test "returns errors when username is already taken", %{conn: conn} do
      create_attrs = Map.merge(@create_attrs, %{device: %{uuid: "another uuid"}})
      conn = conn
        |> post(api_user_path(conn, :create), create_attrs)
      body = json_response(conn, 422)

      assert %{
        "errors" => %{
          "user" => ["username is already taken"]
        }
      } = body
    end
  end

  describe "DELETE /users/:id without basic auth credentials" do
    setup [:setup_user]

    test "prevents access", %{conn: conn, user: user} do
      conn = conn
        |> delete(api_user_path(conn, :delete, user))

      assert response(conn, 401)
    end
  end

  describe "DELETE /users/:id with basic auth credentials" do
    setup [:setup_user, :setup_basic_auth]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = conn
        |> delete(api_user_path(conn, :delete, user))

      assert response(conn, 204)
    end
  end
end
