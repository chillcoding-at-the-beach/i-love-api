defmodule ILoveApiWeb.Admin.ScoreControllerTest do
  use ILoveApiWeb.ConnCase

  alias ILoveApi.Accounts
  alias ILoveApi.Scores

  @create_attrs %{point: 42}
  @update_attrs %{point: 43}
  @invalid_attrs %{user_id: nil, point: nil}

  @basic_auth_username Application.get_env(:i_love_api, :admin_basic_auth)[:username]
  @basic_auth_password Application.get_env(:i_love_api, :admin_basic_auth)[:password]

  defp using_basic_auth(conn, username, password) do
    header_content = "Basic " <> Base.encode64("#{username}:#{password}")
    put_req_header(conn, "authorization", header_content)
  end

  defp valid_attrs do
    {:ok, user} = Accounts.get_or_create_user_by_username("some username", "some uuid")
    Map.merge(@create_attrs, %{user_id: user.id})
  end

  def fixture(:score) do
    {:ok, score} = Scores.create_score(valid_attrs())
    score
  end

  defp setup_basic_auth(context) do
    conn = context[:conn]
      |> using_basic_auth(@basic_auth_username, @basic_auth_password)
    [conn: conn]
  end

  defp setup_score(_) do
    [score: fixture(:score)]
  end

  describe "GET /admin/scores without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> get(admin_score_path(conn, :index))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "GET /admin/scores with basic auth credentials" do
    setup [:setup_basic_auth]

    test "lists all scores", %{conn: conn} do
      conn = conn
        |> get(admin_score_path(conn, :index))

      assert html_response(conn, 200) =~ "Listing Scores"
    end
  end

  describe "GET /admin/scores/new without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> get(admin_score_path(conn, :new))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "GET /admin/scores/new with basic auth credentials" do
    setup [:setup_basic_auth]

    test "renders form", %{conn: conn} do
      conn = conn
        |> get(admin_score_path(conn, :new))

      assert html_response(conn, 200) =~ "New Score"
    end
  end

  describe "POST /admin/scores without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> post(admin_score_path(conn, :create), score: valid_attrs())

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "POST /admin/scores with basic auth credentials" do
    setup [:setup_basic_auth]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = conn
        |> post(admin_score_path(conn, :create), score: valid_attrs())

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == admin_score_path(conn, :show, id)

      conn = recycle(conn)
        |> using_basic_auth(@basic_auth_username, @basic_auth_password)
        |> get(admin_score_path(conn, :show, id))

      assert html_response(conn, 200) =~ "Show Score"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = conn
        |> post(admin_score_path(conn, :create), score: @invalid_attrs)

      assert html_response(conn, 200) =~ "New Score"
    end
  end

  describe "GET /admin/scores/edit without basic auth credentials" do
    setup [:setup_score]

    test "prevents access", %{conn: conn, score: score} do
      conn = conn
        |> get(admin_score_path(conn, :edit, score))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "GET /admin/scores/edit with basic auth credentials" do
    setup [:setup_score, :setup_basic_auth]

    test "renders form for editing chosen score", %{conn: conn, score: score} do
      conn = conn
        |> get(admin_score_path(conn, :edit, score))

      assert html_response(conn, 200) =~ "Edit Score"
    end
  end

  describe "PUT /admin/scores/:id without basic auth credentials" do
    setup [:setup_score]

    test "prevents access", %{conn: conn, score: score} do
      conn = conn
        |> put(admin_score_path(conn, :update, score), score: @update_attrs)

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "PUT /admin/scores/:id with basic auth credentials" do
    setup [:setup_score, :setup_basic_auth]

    test "redirects when data is valid", %{conn: conn, score: score} do
      conn = conn
        |> put(admin_score_path(conn, :update, score), score: @update_attrs)

      assert redirected_to(conn) == admin_score_path(conn, :show, score)

      conn = recycle(conn)
        |> using_basic_auth(@basic_auth_username, @basic_auth_password)
        |> get(admin_score_path(conn, :show, score))

      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, score: score} do
      conn = conn
        |> put(admin_score_path(conn, :update, score), score: @invalid_attrs)

      assert html_response(conn, 200) =~ "Edit Score"
    end
  end

  describe "DELETE /admin/scores/:id without basic auth credentials" do
    setup [:setup_score]

    test "prevents access", %{conn: conn, score: score} do
      conn = conn
        |> delete(admin_score_path(conn, :delete, score))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "DELETE /admin/scores/:id with basic auth credentials" do
    setup [:setup_score, :setup_basic_auth]

    test "deletes chosen score", %{conn: conn, score: score} do
      conn = conn
        |> delete(admin_score_path(conn, :delete, score))

      assert redirected_to(conn) == admin_score_path(conn, :index)
      assert_error_sent 404, fn ->
        recycle(conn)
        |> using_basic_auth(@basic_auth_username, @basic_auth_password)
        |> get(admin_score_path(conn, :show, score))
      end
    end
  end
end
