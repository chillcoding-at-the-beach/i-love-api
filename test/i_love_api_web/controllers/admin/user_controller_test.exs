defmodule ILoveApiWeb.Admin.UserControllerTest do
  use ILoveApiWeb.ConnCase

  alias ILoveApi.Accounts
  alias ILoveApi.Devices

  @create_attrs %{username: "some username", device_id: "some uuid"}
  @update_attrs %{username: "some updated username"}
  @invalid_attrs %{username: nil, device_id: nil}

  @basic_auth_username Application.get_env(:i_love_api, :admin_basic_auth)[:username]
  @basic_auth_password Application.get_env(:i_love_api, :admin_basic_auth)[:password]

  defp using_basic_auth(conn, username, password) do
    header_content = "Basic " <> Base.encode64("#{username}:#{password}")
    put_req_header(conn, "authorization", header_content)
  end

  defp valid_attrs do
    {:ok, device} = Devices.create_device(%{uuid: "some uuid"})
    Map.merge(@create_attrs, %{device_id: device.id})
  end

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(valid_attrs())
    user
  end

  defp setup_basic_auth(context) do
    conn = context[:conn]
      |> using_basic_auth(@basic_auth_username, @basic_auth_password)
    [conn: conn]
  end

  defp setup_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end

  describe "GET /admin/users without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> get(admin_user_path(conn, :index))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "GET /admin/users with basic auth credentials" do
    setup [:setup_basic_auth]

    test "lists all users", %{conn: conn} do
      conn = conn
        |> get(admin_user_path(conn, :index))

      assert html_response(conn, 200) =~ "Listing Users"
    end
  end

  describe "GET /admin/users/new without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> get(admin_user_path(conn, :new))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "GET /admin/users/new with basic auth credentials" do
    setup [:setup_basic_auth]

    test "renders form", %{conn: conn} do
      conn = conn
        |> get(admin_user_path(conn, :new))

      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "POST /admin/users without basic auth credentials" do
    test "prevents access", %{conn: conn} do
      conn = conn
        |> post(admin_user_path(conn, :create), user: valid_attrs())

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "POST /admin/users with basic auth credentials" do
    setup [:setup_basic_auth]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = conn
        |> post(admin_user_path(conn, :create), user: valid_attrs())

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == admin_user_path(conn, :show, id)

      conn = recycle(conn)
        |> using_basic_auth(@basic_auth_username, @basic_auth_password)
        |> get(admin_user_path(conn, :show, id))

      assert html_response(conn, 200) =~ "Show User"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = conn
        |> post(admin_user_path(conn, :create), user: @invalid_attrs)

      assert html_response(conn, 200) =~ "New User"
    end
  end

  describe "GET /admin/users/:id/edit without basic auth credentials" do
    setup [:setup_user]

    test "prevents access", %{conn: conn, user: user} do
      conn = conn
        |> get(admin_user_path(conn, :edit, user))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "GET /admin/users/:id/edit with basic auth credentials" do
    setup [:setup_user, :setup_basic_auth]

    test "renders form for editing chosen user", %{conn: conn, user: user} do
      conn = conn
        |> get(admin_user_path(conn, :edit, user))

      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "PUT /admin/users/:id without basic auth credentials" do
    setup [:setup_user]

    test "prevents access", %{conn: conn, user: user} do
      conn = conn
        |> put(admin_user_path(conn, :update, user), user: @update_attrs)

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "PUT /admin/users/:id with basic auth credentials" do
    setup [:setup_user, :setup_basic_auth]

    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = conn
        |> put(admin_user_path(conn, :update, user), user: @update_attrs)

      assert redirected_to(conn) == admin_user_path(conn, :show, user)

      conn = recycle(conn)
        |> using_basic_auth(@basic_auth_username, @basic_auth_password)
        |> get(admin_user_path(conn, :show, user))

      assert html_response(conn, 200) =~ "some updated username"
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = conn
        |> put(admin_user_path(conn, :update, user), user: @invalid_attrs)

      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "DELETE /admin/users/:id without basic auth credentials" do
    setup [:setup_user]

    test "prevents access", %{conn: conn, user: user} do
      conn = conn
        |> delete(admin_user_path(conn, :delete, user))

      assert response(conn, 401) =~ "401 Unauthorized"
    end
  end

  describe "DELETE /admin/users/:id with basic auth credentials" do
    setup [:setup_user, :setup_basic_auth]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = conn
        |> delete(admin_user_path(conn, :delete, user))

      assert redirected_to(conn) == admin_user_path(conn, :index)
      assert_error_sent 404, fn ->
        recycle(conn)
        |> using_basic_auth(@basic_auth_username, @basic_auth_password)
        |> get(admin_user_path(conn, :show, user))
      end
    end
  end
end
