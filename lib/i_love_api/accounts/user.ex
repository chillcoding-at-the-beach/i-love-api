defmodule ILoveApi.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :username, :string
    belongs_to :device, ILoveApi.Devices.Device
    has_many :scores, ILoveApi.Scores.Score

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:device_id, :username])
    |> validate_required([:device_id, :username])
    |> unique_constraint(:username)
  end
end
