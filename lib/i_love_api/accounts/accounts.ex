defmodule ILoveApi.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias ILoveApi.Repo

  alias ILoveApi.Accounts.User
  alias ILoveApi.Devices

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
    |> Repo.preload([:device, :scores])
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123, 789)
      %User{}

      iex> get_user!(456, 789)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id) do
    User
    |> Repo.get!(id)
    |> Repo.preload([:device, :scores])
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123, 789)
      %User{}

      iex> get_user!(456, 789)
      ** (Ecto.NoResultsError)

  """
  def get_user(id, uuid) do
    case Repo.get_by(User, id: id)
         |> Repo.preload([:device, :scores]) do
      nil -> {:error, :no_user}
      %User{} = user ->
        case user.uuid do
          ^uuid -> {:ok, user}
          _ -> {:error, :wrong_uuid}
        end
    end
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    change_user(attrs)
    |> Repo.insert()
  end

  def get_or_create_user_by_username(_username = nil, _uuid) do
    {:error, :no_username}
  end

  def get_or_create_user_by_username(_username, _uuid = nil) do
    {:error, :no_uuid}
  end

  @doc """
  Gets or creates a user.

  ## Examples

      iex> get_or_create_user_by_username(username, uuid)
      {:ok, %User{}}

      iex> get_or_create_user_by_username(username, uuid)
      {:error, :wrong_uuid}

      iex> get_or_create_user_by_username(username, uuid)
      {:error, %Ecto.Changeset{}}

  """
  def get_or_create_user_by_username(username, uuid) do
    case Repo.get_by(User, username: username) do
      nil ->
        {:ok, device} = Devices.get_or_create_device_by_uuid(uuid)
        {:ok, user} = create_user(%{username: username, device_id: device.id})
        user = Repo.get_by(User, id: user.id)
               |> Repo.preload([:device, :scores])

        {:ok, user}
      user ->
        user = Repo.preload(user, [:device, :scores])
        if user.device.uuid == uuid do
          {:ok, user}
        else
          {:error, :wrong_uuid}
        end
    end
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    change_user(user, attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(%{username: "johndoe"})
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(attrs) do
    User.changeset(%User{}, attrs)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user, attrs) do
    User.changeset(user, attrs)
  end
end
