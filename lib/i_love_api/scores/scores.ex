defmodule ILoveApi.Scores do
  @moduledoc """
  The Scores context.
  """

  import Ecto.Query, only: [from: 2]
  alias ILoveApi.Repo

  alias ILoveApi.Scores.Score
  alias ILoveApi.Accounts.User

  @doc """
  Returns the list of scores.

  ## Examples

      iex> list_scores()
      [%Score{}, ...]

  """
  def list_scores do
    query = from s in Score,
              order_by: [desc: :point]

    Repo.all(query)
    |> Repo.preload([user: [:device]])
  end

  @doc """
  Gets a single score.

  Raises `Ecto.NoResultsError` if the Score does not exist.

  ## Examples

      iex> get_score!(123)
      %Score{}

      iex> get_score!(456)
      ** (Ecto.NoResultsError)

  """
  def get_score!(id) do
    Score
    |> Repo.get!(id)
    |> Repo.preload([user: [:device]])
  end

  @doc """
  Creates a score.

  ## Examples

      iex> create_score(%{field: value})
      {:ok, %Score{}}

      iex> create_score(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_score(attrs \\ %{}) do
    case %Score{}
      |> Score.changeset(attrs)
      |> Repo.insert() do
        {:ok, score} ->
          score = score
            |> Repo.preload([user: [:device]])

          {:ok, score}
        any -> any
      end
  end

  @doc """
  Creates a score.

  ## Examples

      iex> create_score(%User{}, %{field: value})
      {:ok, %Score{}}

      iex> create_score(%User{}, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_score(%User{} = user, attrs) do
    attrs = Map.merge(attrs, %{user_id: user.id})

    case %Score{}
      |> Score.changeset(attrs)
      |> Repo.insert() do
      {:ok, score} ->
        {:ok, Repo.preload(score, [user: [:device]])}
      any -> any
    end
  end

  @doc """
  Updates a score.

  ## Examples

      iex> update_score(score, %{field: new_value})
      {:ok, %Score{}}

      iex> update_score(score, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_score(%Score{} = score, attrs) do
    score
    |> Score.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Score.

  ## Examples

      iex> delete_score(score)
      {:ok, %Score{}}

      iex> delete_score(score)
      {:error, %Ecto.Changeset{}}

  """
  def delete_score(%Score{} = score) do
    Repo.delete(score)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking score changes.

  ## Examples

      iex> change_score(score)
      %Ecto.Changeset{source: %Score{}}

  """
  def change_score(%Score{} = score) do
    Score.changeset(score, %{})
  end
end
