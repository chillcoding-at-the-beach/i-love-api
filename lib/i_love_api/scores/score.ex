defmodule ILoveApi.Scores.Score do
  use Ecto.Schema
  import Ecto.Changeset

  schema "scores" do
    field :point, :integer
    belongs_to :user, ILoveApi.Accounts.User
    has_one :device, through: [:user, :device_id]

    timestamps()
  end

  @doc false
  def changeset(score, attrs) do
    score
    |> cast(attrs, [:user_id, :point])
    |> validate_required([:user_id, :point])
  end
end
