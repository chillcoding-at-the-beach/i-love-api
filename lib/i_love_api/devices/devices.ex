defmodule ILoveApi.Devices do
  @moduledoc """
  The Scores context.
  """

  import Ecto.Query, warn: false
  alias ILoveApi.Repo

  alias ILoveApi.Devices.Device

  @doc """
  Creates a device.

  ## Examples

      iex> create_device(%{field: value})
      {:ok, %User{}}

      iex> create_device(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_device(attrs \\ %{}) do
    %Device{}
    |> Device.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Gets or creates a device.

  ## Examples

      iex> get_or_create_device_by_uuid(uuid)
      {:ok, %Device{}}

      iex> get_or_create_device_by_uuid(uuid)
      {:error, %Ecto.Changeset{}}

  """
  def get_or_create_device_by_uuid(uuid) do
    case Repo.get_by(Device, uuid: uuid) do
      nil -> create_device(%{uuid: uuid})
      device -> {:ok, device}
    end
  end
end
