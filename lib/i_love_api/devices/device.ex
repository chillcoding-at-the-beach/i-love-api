defmodule ILoveApi.Devices.Device do
  use Ecto.Schema
  import Ecto.Changeset

  schema "devices" do
    field :uuid, :string
    has_one :user, ILoveApi.Accounts.User
    has_many :scores, through: [:user, :scores]

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:uuid])
    |> validate_required([:uuid])
    |> unique_constraint(:uuid)
  end
end
