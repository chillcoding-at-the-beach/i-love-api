defmodule ILoveApiWeb.Router do
  use ILoveApiWeb, :router

  pipeline :admin do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug BasicAuth, use_config: {:i_love_api, :admin_basic_auth}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope path: "/admin", as: :admin, alias: ILoveApiWeb do
    pipe_through :admin

    resources "/users", Admin.UserController
    resources "/scores", Admin.ScoreController
  end

  # Other scopes may use custom stacks.
  scope path: "/api", as: :api, alias: ILoveApiWeb do
    pipe_through :api

    resources "/users", Api.UserController, except: [:new, :edit]
    resources "/scores", Api.ScoreController, except: [:new, :edit]
  end
end
