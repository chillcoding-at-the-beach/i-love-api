defmodule ILoveApiWeb.Admin.UserView do
  use ILoveApiWeb, :view

  import Ecto.Query, only: [from: 2]

  alias ILoveApi.Repo
  alias ILoveApi.Accounts.User
  alias ILoveApi.Scores.Score

  def user_best_score(%User{} = user) do
    query =
      from sl in Score,
        where: sl.user_id == ^user.id
    Repo.aggregate(query, :max, :point)
  end
end
