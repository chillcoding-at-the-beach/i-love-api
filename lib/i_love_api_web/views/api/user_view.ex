defmodule ILoveApiWeb.Api.UserView do
  use ILoveApiWeb, :view
  alias ILoveApiWeb.Api.UserView

  def render("index.json", %{conn: conn, users: users}) do
    render_many(users, UserView, "user.json", %{conn: conn})
  end

  def render("show.json", %{conn: conn, user: user}) do
    render_one(user, UserView, "user.json", %{conn: conn})
  end

  def render("user.json", %{conn: conn, user: user}) do
    %{
      id: user.id,
      username: user.username,
      device: %{
        id: user.device.id,
        uuid: user.device.uuid
      },
      _links: %{
        self: api_user_path(conn, :show, user)
      }
    }
  end
end
