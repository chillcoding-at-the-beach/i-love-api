defmodule ILoveApiWeb.Api.ScoreView do
  use ILoveApiWeb, :view
  alias ILoveApiWeb.Api.ScoreView

  def render("index.json", %{conn: conn, scores: scores}) do
    render_many(scores, ScoreView, "score.json", %{conn: conn})
  end

  def render("show.json", %{conn: conn, score: score}) do
    render_one(score, ScoreView, "score.json", %{conn: conn})
  end

  def render("score.json", %{conn: conn, score: score}) do
    %{
      id: score.id,
      point: score.point,
      device: %{
        id: score.user.device.id,
        uuid: score.user.device.uuid
      },
      user: %{
        id: score.user.id,
        username: score.user.username
      },
      _links: %{
        self: api_score_path(conn, :show, score)
      }
    }
  end
end
