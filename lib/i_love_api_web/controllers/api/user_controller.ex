defmodule ILoveApiWeb.Api.UserController do
  use ILoveApiWeb, :controller

  alias ILoveApi.Accounts.User
  alias ILoveApi.Accounts

  plug BasicAuth, [use_config: {:i_love_api, :admin_basic_auth}] when not action in [:index, :show]

  action_fallback ILoveApiWeb.Api.FallbackController

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.json", conn: conn, users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.json", conn: conn, user: user)
  end

  def create(conn, %{"device" => %{"uuid" => uuid}, "user" => %{"username" => username}}) do
    with {:ok, %User{} = user} <- Accounts.get_or_create_user_by_username(username, uuid) do
      render(conn, "show.json", conn: conn, user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    with {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
