defmodule ILoveApiWeb.Api.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use ILoveApiWeb, :controller

  alias ILoveApi.Scores.Score

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    changeset = case changeset.data do
      %Score{} ->
        case elem(changeset.errors[:point], 0) do
          "can't be blank" ->
            Ecto.Changeset.cast({%{}, %{}}, %{}, [])
            |> Ecto.Changeset.add_error(:"score", "point can't be blank")
          _ -> changeset
        end
        _ -> changeset
    end
    conn
    |> put_status(:unprocessable_entity)
    |> render(ILoveApiWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :no_uuid}) do
    changeset = Ecto.Changeset.cast({%{}, %{}}, %{}, [])
        |> Ecto.Changeset.add_error(:device, "uuid can't be blank")

    conn
    |> put_status(:unprocessable_entity)
    |> render(ILoveApiWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :no_username}) do
    changeset = Ecto.Changeset.cast({%{}, %{}}, %{}, [])
        |> Ecto.Changeset.add_error(:user, "username can't be blank")

    conn
    |> put_status(:unprocessable_entity)
    |> render(ILoveApiWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :wrong_uuid}) do
    changeset = Ecto.Changeset.cast({%{}, %{}}, %{}, [])
    |> Ecto.Changeset.add_error(:user, "username is already taken")

    conn
    |> put_status(:unprocessable_entity)
    |> render(ILoveApiWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(ILoveApiWeb.ErrorView, :"404")
  end
end
