defmodule ILoveApiWeb.Api.ScoreController do
  use ILoveApiWeb, :controller

  alias ILoveApi.Scores
  alias ILoveApi.Scores.Score
  alias ILoveApi.Accounts
  alias ILoveApi.Accounts.User

  plug BasicAuth, [use_config: {:i_love_api, :admin_basic_auth}] when not action in [:index, :show]

  action_fallback ILoveApiWeb.Api.FallbackController

  def index(conn, _params) do
    scores = Scores.list_scores()
    render(conn, "index.json", conn: conn, scores: scores)
  end

  def create(conn, %{"device" => %{"uuid" => uuid}, "user" => %{"username" => username}, "score" => %{"point" => point}}) do
    with {:ok, %User{} = user} <- Accounts.get_or_create_user_by_username(username, uuid),
      {:ok, %Score{} = score} <- Scores.create_score(user, %{point: point}) do
        conn
        |> put_status(:created)
        |> render("show.json", conn: conn, score: score)
    end
  end

  def show(conn, %{"id" => id}) do
    score = Scores.get_score!(id)
    render(conn, "show.json", conn: conn, score: score)
  end

  def delete(conn, %{"id" => id}) do
    score = Scores.get_score!(id)

    with {:ok, %Score{}} = Scores.delete_score(score) do
      send_resp(conn, :no_content, "")
    end
  end
end
