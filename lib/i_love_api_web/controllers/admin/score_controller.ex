defmodule ILoveApiWeb.Admin.ScoreController do
  use ILoveApiWeb, :controller

  alias ILoveApi.Scores.Score
  alias ILoveApi.Scores
  alias ILoveApi.Accounts

  def index(conn, _params) do
    scores = Scores.list_scores()
    render(conn, "index.html", scores: scores)
  end

  def new(conn, _params) do
    changeset = Scores.change_score(%Score{})
    users = Accounts.list_users()
    render(conn, "new.html", changeset: changeset, users: users)
  end

  def create(conn, %{"score" => score_params}) do
    case Scores.create_score(score_params) do
      {:ok, score} ->
        conn
        |> put_flash(:info, "Score created successfully.")
        |> redirect(to: admin_score_path(conn, :show, score))
      {:error, %Ecto.Changeset{} = changeset} ->
        users = Accounts.list_users()
        render(conn, "new.html", changeset: changeset, users: users)
    end
  end

  def show(conn, %{"id" => id}) do
    score = Scores.get_score!(id)
    render(conn, "show.html", score: score)
  end

  def edit(conn, %{"id" => id}) do
    score = Scores.get_score!(id)
    changeset = Scores.change_score(score)
    users = Accounts.list_users()
    render(conn, "edit.html", score: score, changeset: changeset, users: users)
  end

  def update(conn, %{"id" => id, "score" => score_params}) do
    score = Scores.get_score!(id)

    case Scores.update_score(score, score_params) do
      {:ok, score} ->
        conn
        |> put_flash(:info, "Score updated successfully.")
        |> redirect(to: admin_score_path(conn, :show, score))
      {:error, %Ecto.Changeset{} = changeset} ->
        users = Accounts.list_users()
        render(conn, "edit.html", score: score, changeset: changeset, users: users)
    end
  end

  def delete(conn, %{"id" => id}) do
    score = Scores.get_score!(id)
    {:ok, _score} = Scores.delete_score(score)

    conn
    |> put_flash(:info, "Score deleted successfully.")
    |> redirect(to: admin_score_path(conn, :index))
  end
end
