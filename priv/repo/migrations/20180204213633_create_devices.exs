defmodule ILoveApi.Repo.Migrations.CreateDevices do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :uuid, :string

      timestamps()
    end

    create unique_index(:devices, [:uuid])
  end
end
