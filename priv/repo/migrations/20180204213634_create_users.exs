defmodule ILoveApi.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :device_id, references(:devices, on_delete: :delete_all), null: false
      add :username, :string

      timestamps()
    end

    create unique_index(:users, [:username])
  end
end
