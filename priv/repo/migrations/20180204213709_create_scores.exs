defmodule ILoveApi.Repo.Migrations.CreateScores do
  use Ecto.Migration

  def change do
    create table(:scores) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :point, :integer

      timestamps()
    end

    create index(:scores, [:user_id])
  end
end
