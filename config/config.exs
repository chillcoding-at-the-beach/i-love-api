# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :i_love_api,
  ecto_repos: [ILoveApi.Repo]

# Configures the endpoint
config :i_love_api, ILoveApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "BsSzuYD4hw+cvO9s5ZosMB4G1k0/oI22ZdKwcd+GKq97AYRFeZk3ZEa2/Mhaj0aC",
  render_errors: [view: ILoveApiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ILoveApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :i_love_api, admin_basic_auth: [
  username: "admin",
  password: "admin",
  realm: "Admin Area"
]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
